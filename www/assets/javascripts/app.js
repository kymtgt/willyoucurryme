$(function(){
  var loaded = false;
  var $loading = $("#loading");
  var t1 = new Date();
  var $enter = $("#enter");
  var $main = $("#main");
  var w = $(window).height();
  $enter.height(w);

  $enter.on('touchstart click', function(){
    event.preventDefault();
    if(loaded){
      enter();
    }
  });


  var $upas = $("#upas");
  var $tsuguto1 = $("#tsuguto1");
  var tsuguTop = $tsuguto1.offset().top + 60;
  var $itsumi1 = $("#itsumi1");
  var itsuTop1 = $itsumi1.offset().top + 60;
  var $itsumi2 = $("#itsumi2");
  var itsuTop2 = $itsumi2.offset().top + 100;
  var wt = $(window).scrollTop();
  $(window).on('scroll', function(){
    wt = $(window).scrollTop();
    $upas.css({marginTop: wt+'px'});

    if($tsuguto1.hasClass('in')){
      if(wt > tsuguTop - w) {
        $tsuguto1.removeClass('in');
      }
    }
    if($itsumi1.hasClass('in')){
      if(wt > itsuTop1 - w) {
        $itsumi1.removeClass('in');
      }
    }
    if($itsumi2.hasClass('in')){
      if(wt > itsuTop2 - w) {
        $itsumi2.removeClass('in');
      }
    }
  });

  $(window).on("load", function(){
    loaded = true;
    $loading.addClass('out');
    var t2 = new Date();
    var d = t2 - t1;
    if(d > 2000) {
      enter();
    }
  });


  var enter = function(){
    $enter.addClass("entering");
    setTimeout(function(){
      $enter.removeClass("entering").addClass("entered");
      $main.removeClass("entering").addClass("entered");
      $enter.remove();
    }, 200)
  }


})